import pyautogui
import time

ruleta_col_x=950
ruleta_col_y=870

pelota_x=950
pelota_y=360

ruleta_x_min=895
ruleta_y_min=870

def obtener_color_pixel(x,y):
    pixel = pyautogui.pixel(x,y)
    return pixel

color_fondo=(253, 252 , 245)

colores_conocidos=[(239,46,19),(133,28,197),(242,218,0),(248,142,31),(129,158,141),(0,77,242),color_fondo]
colores_ruleta=[(239,46,19),(133,28,197),(248,142,31),(129,158,141),(242,218,0),(0,77,242)]
color_encontrado=color_fondo

def calcular_clics_color(color_objetivo):
    indice_actual = obtener_indice_color(obtener_color_pixel(ruleta_col_x, ruleta_col_y))
    indice_objetivo = obtener_indice_color(color_objetivo)
    distancia =  indice_actual - indice_objetivo
    if distancia == 0:
        return None
    elif distancia < 0:
        distancia += len(colores_ruleta)
    return distancia

def obtener_indice_color(color):
    indice = colores_ruleta.index(color)
    return indice

print("3")
color_encontrado=obtener_color_pixel(pelota_x,pelota_y)
time.sleep(1)
print("2")
time.sleep(1)
print ("1")
time.sleep(1)
print ("GO")
pyautogui.click(x=ruleta_x_min,y=ruleta_y_min)

def make_clicks(clics_necesarios):
    if clics_necesarios is not None:
        for click in range(clics_necesarios):
            pyautogui.click(x=ruleta_x_min,y=ruleta_y_min)

while True:
    while color_encontrado == color_fondo:
        color_encontrado=obtener_color_pixel(pelota_x,pelota_y)
        if color_encontrado not in colores_conocidos:
            color_encontrado=color_fondo
    while obtener_color_pixel(ruleta_col_x, ruleta_col_y) != color_encontrado:
        clics_necesarios = calcular_clics_color(color_encontrado)
        make_clicks(clics_necesarios)
    color_encontrado=color_fondo
