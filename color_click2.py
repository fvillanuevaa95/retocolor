#import cv2
import pyautogui
import random
import time

ruleta_col_x=950
ruleta_col_y=870

pelota_x=950
pelota_y=360

ruleta_x_min=895
ruleta_x_max=1005
ruleta_y_min=870
ruleta_y_max=980

def obtener_color_pixel(x,y):
    pixel = pyautogui.pixel(x,y)
    return pixel

color=obtener_color_pixel(950,365)
color_fondo=(253, 252 , 245)
if color == color_fondo:
    print("Hola")

colores_conocidos=[(239,46,19),(133,28,197),(242,218,0),(248,142,31),(129,158,141),(0,77,242),color_fondo]
color_encontrado=color_fondo

while True:
    print("Empiezo")
    while color_encontrado == color_fondo:
        color_encontrado=obtener_color_pixel(pelota_x,pelota_y)
        if color_encontrado not in colores_conocidos:
            print("FALLO")
            print(color_encontrado)
            color_encontrado=color_fondo
    print(f"He encontrado {color_encontrado}")
    color_ruleta=obtener_color_pixel(ruleta_col_x,ruleta_col_y)
    x_random=random.randint(ruleta_x_min,ruleta_x_max)
    y_random=random.randint(ruleta_y_min,ruleta_y_max)
    while color_ruleta != color_encontrado:
        pyautogui.click(x=x_random,y=y_random)
        #time_random=random.uniform(0.005,0.01)
        #time.sleep(time_random)
        color_ruleta=obtener_color_pixel(ruleta_col_x,ruleta_col_y)
        print(f"La ruleta es {color_ruleta}")
    color_encontrado=color_fondo
